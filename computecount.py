import math


class Compute:

    """Creo en el constructor una variable contador, inicializada en cero,
    de tal manera que, cada vez que se llame a un método de la clase, se suma 1"""
    def __init__(self, count=0):
        self.count = count

    """Los métodos tienen definido como segundo parámetro un valor por defecto,que será la
     base o exponente si no se especifica"""
    def power(self, num1, num2=2):
        self.count = self.count + 1
        return num1 ** num2

    def log(self, num1, num2=2):
        self.count = self.count + 1
        return math.log(num1, num2)

    def set_def(self, num2=2):
        self.count = self.count + 1
        if num2 <= 0:
            raise ValueError("No se acepta una base o exponente menor que cero")

    def get_def(self, num2):
        self.count = self.count + 1
        return num2
