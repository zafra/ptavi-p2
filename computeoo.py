import math
import sys


class Compute:

    """Creamos un constructor para declarar las variables
       exponente, base y numero"""
    def __init__(self, arg1, arg2):
        self.exponente = arg2
        self.base = arg2
        self.numero = arg1

    def power(self):
        return self.numero ** self.exponente

    def log(self):
        return math.log(self.numero, self.base)


if __name__ == "__main__":

    """El programa realizará diferentes acciones en función del número de argumentos 
       pasados por línea de comandos"""
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")
    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    """Se crea un objeto que instancia a la clase Compute, pasándole los parámetros
    que acepta esta clase"""
    miOperacion = Compute(num, num2)

    if sys.argv[1] == "power":
        result = miOperacion.power()
    elif sys.argv[1] == "log":
        result = miOperacion.log()
    else:
        sys.exit('Operand should be power or log')

    print(result)
