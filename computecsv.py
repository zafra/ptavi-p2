import math


class Compute:
    def __init__(self, count=0):
        self.fichero = None
        self.count = count

    def power(self, num1, num2):
        self.count = self.count + 1
        return num1 ** num2

    def log(self, num1, num2):
        self.count = self.count + 1
        return math.log(num1, num2)

    def operations(self, operand, operand1, operand2):
        if operand == "power":
            return self.power(operand1, operand2)
        if operand == "log":
            return self.log(operand1, operand2)
        elif operand >= 0:
            print(f"Default: {operand}")
        else:
            print("Error: default value must be greater than zero")

    def process_csv(self, fichero):
        computa = Compute()
        self.fichero = fichero
        with open(self.fichero) as file:
            lineas = file.readlines()
            for linea in lineas:
                linea = linea.rstrip('\n')
                lista = linea.split(',')
                try:
                    operator, op1, op2 = lista[0], lista[1], lista[2]
                    resultado = computa.operations(operator, float(op1), float(op2))
                    print(resultado)
                except IndexError:
                    print("Bad format")

        computa.process_csv('fichero.txt')
