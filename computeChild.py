import computeoo
import sys

"""creo una clase que hereda la clase de Compute y todos
    los métodos definidos en ella"""


class ComputeChild(computeoo.Compute):

    """Creo un constructor que hace uso de super para poder
    usar los mismos parametros que la clase padre"""

    def __init__(self, op1, op2):
        super().__init__(op1, op2)
        self.default = op2

    def set_def(self):
        if self.default <= 0:
            raise ValueError("No se acepta una base o exponente menor que cero")

    def get_def(self):
        return self.default


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")
    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    """Al objeto le paso como segundo parámetro un valor fijo, de tal manera
    que, cuando no se escribe un tercer argumento en la línea de comandos, 
    lo toma por defecto como la base o el exponente"""
    miOperacion = ComputeChild(num, 2)

    if len(sys.argv) > 3:
        try:
            num2 = float(sys.argv[3])
            miOperacion = ComputeChild(num, num2)
        except ValueError:
            sys.exit("Error: third argument should be a number")

    miOperacion.set_def()
    miOperacion.get_def()

    if sys.argv[1] == "power":
        result = miOperacion.power()
    elif sys.argv[1] == "log":
        result = miOperacion.log()
    else:
        sys.exit('Operand should be power or log')

    print(result)
